const goods = [
  {
    img: './images/Multimetr/klesh mt-87.jfif',
    title: 'Мультиметр клещ MT-87',
    desc: ``,
    prize: 5.21,
    type: 'multimetr',
    sold: 3
  },
  {
    img: './images/Multimetr/Multimetr klesh DT-266.jfif',
    title: 'Мультиметр клещ DT-266',
    desc: ``,
    prize: 6.08,
    type: 'multimetr',
    sold: 3
  },
  {
    img: './images/Multimetr/Multimetr DT-9205A.jfif',
    title: 'Multimetr DT-9205A',
    desc: ``,
    prize: 6.45,
    type: 'multimetr',
    sold: 3
  },
  {
    img: './images/Multimetr/multimetr VC-9205AL.jfif',
    title: 'Мультиметр VC-9205AL',
    desc: ``,
    prize: 9.73,
    sold: 2,
    type: 'multimetr'
  },
  {
    img: './images/Multimetr/multimetr DT-830B.jfif',
    title: 'Мультиметр DT-830B',
   desc: ``,
    prize: 2.86,
    type: 'multimetr',
    sold: 1
  },
  {
    img: './images/Multimetr/multimetr DT-830D.jfif',
    title: 'Мультиметр DT-830D',
    desc: ``,
    prize: 3.06,
    type: 'multimetr',
    sold: 4
  },
  {
    img: './images/Multimetr/multimetr MAS-830L.jfif',
    title: 'Мультиметр MAS-830L',
    desc: ``,
    prize: 6.43,
    type: 'multimetr',
    sold: 4
  },
  {
    img: './images/Multimetr/multimetr DT-700.jfif',
    title: 'Мультиметр DT-700',
    desc: ``,
    prize: 4.82,
    type: 'multimetr',
    sold: 4
  },
  {
    img: './images/Multimetr/multimedia LAN TESTER NS-468.jfif',
    title: 'multimetr LAN TESTER NS-468',
    desc: ``,
    prize: 2.71,
    type: 'multimetr',
    sold: 4
  },
  {
    img: './images/Multimetr/multimetr LAN TESTER BS479468.jfif',
    title: 'Мультиметр LAN TESTER BS479468',
    desc: ``,
    prize: 7.25,
    type: 'multimetr',
    sold: 4
  },
  {
    img: './images/Multimetr/multimetr MT-182.jfif',
    title: 'Мультиметр MT-182',
    desc: ``,
    prize: 3.72,
    type: 'multimetr',
    sold: 4
  },
  {
    img: './images/Multimetr/multimetr UNI-T UT33A.jfif',
    title: 'Мультиметр UNI-T UT33A',
    desc: ``,
    prize: 16.21,
    type: 'multimetr',
    sold: 4
  },
  {
    img: './images/Multimetr/multmetr UNI-T UT33C+.jfif',
    title: 'Мультиметр UNI-T UT33C+',
    desc: ``,
    prize: 16.21,
    type: 'multimetr',
    sold: 4
  },
  {
    img: './images/Multimetr/multimetr UNI-T UT33B+.jfif',
    title: 'Мультиметр UNI-T UT33B+',
    desc: ``,
    prize: 12.35,
    type: 'multimetr',
    sold: 4
  },
  {
    img: './images/Multimetr/multimetr UNI-T UT33D+.jfif',
    title: 'Мультиметр UNI-T UT33D+',
    desc: ``,
    prize: 12.60,
    type: 'multimetr',
    sold: 4
  },
  {
    img: './images/Multimetr/multimetr UNI-T UT-51.jfif',
    title: 'Мультиметр UNI-T UT-51',
    desc: ``,
    prize: 25.17,
    type: 'multimetr',
    sold: 4
  },
  {
    img: './images/Multimetr/multimetr UNI-T UT201.jfif',
    title: 'Мультиметр UNI-T UT201',
    desc: ``,
    prize: 25.17,
    type: 'multimetr',
    sold: 4
  },
  
  //---KABEL---
  {
    img: './images/Multimetr/LAN CABLE RG45 CAT6 1M-10M.jfif',
    title: 'LAN CABLE RG45 CAT6 2M',
    desc: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi quaerat nobis illo laborum explicabo vero recusandae quos esse sed! `,
    prize: 0.79,
    type: 'kabel',
    sold: 5
  },
  {
    img: './images/Multimetr/LAN CABLE RG45 CAT6 1M-10M.jfif',
    title: 'LAN CABLE RG45 CAT6 3M',
  desc: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi quaerat nobis illo laborum explicabo vero recusandae quos esse sed! `,
    prize: 0.79,
    type: 'kabel',
    sold: 5
  },
  {
    img: './images/Multimetr/LAN CABLE RG45 CAT6 1M-10M.jfif',
    title: 'LAN CABLE RG45 CAT6 5M',
  desc: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi quaerat nobis illo laborum explicabo vero recusandae quos esse sed! `,
    prize: 0.79,
    type: 'kabel',
    sold: 5
  },
  {
    img: './images/Multimetr/LAN CABLE RG45 CAT6 1M-10M.jfif',
    title: 'LAN CABLE RG45 CAT6 10M',
    desc: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi quaerat nobis illo laborum explicabo vero recusandae quos esse sed! `,
    prize: 0.79,
    type: 'kabel',
    sold: 5
  },
  {
    img: './images/Multimetr/Multimetr DT-9205A.jfif',
    title: 'Multimetr DT-9205A',
    desc: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi quaerat nobis illo laborum explicabo vero recusandae quos esse sed! `,
    prize: 15.99,
    type: 'multimetr',
    sold: 87

  },
  {
    img: './images/Multimetr/Multimetr DT-9205A.jfif',
    title: 'Multimetr DT-9205A',
    desc: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi quaerat nobis illo laborum explicabo vero recusandae quos esse sed! `,
    prize: 15.99,
    type: 'multimetr',
    sold: 201

  },
  //---KAMERA---
  {
    img: './images/Multimetr/JVS-A815-C1.jfif',
    title: 'JVS-A815-C1 2MP AHD DOME CAM, PLASTIC, PAL',
    desc: ``,
    prize: 16.59,
    type: 'kamera',
    sold: 4

  },
  {
    img: './images/Multimetr/JVS-A835-C1.jfif',
    title: 'JVS-A835-C1 2MP AHD BULLET CAM, PLASTIC, PAL',
    desc: ``,
    prize: 16.59,
    type: 'kamera',
    sold: 4

  },
  {
    img: './images/Multimetr/JVS-A815-YWC.jfif',
    title: 'JVS-A815-YWC 2MP AHD DOME CAM, PLASTIC, PAL',
    desc: ``,
    prize: 16.59,
    type: 'kamera',
    sold: 4

  },
  {
    img: './images/Multimetr/JVS-ND6608-HD.jfif',
    title: 'JVS-ND6608-HD',
    desc: ``,
    prize: 62.61,
    type: 'kamera',
    sold: 1
  },
  {
    img: './images/Multimetr/JVS-ND6616-HC2.jfif',
    title: 'JVS-ND6616-HC2',
    desc: ``,
    prize: 78.25,
    type: 'kamera',
    sold: 1
  },
  {
    img: './images/Multimetr/JVS-ND7932-HV.jfif',
    title: 'JVS-ND7932-HV',
    desc: ``,
    prize: 201.49,
    type: 'kamera',
    sold: 1
  },
  {
    img: './images/Multimetr/JVS-DA230.jfif',
    title: '2MP Wi-Fi Cube camera Built-in MIC & speaker and audio SD card upto 128GB',
    desc: ``,
    prize: 40.16,
    type: 'kamera',
    sold: 2
  },
  {
    img: './images/Multimetr/536 2MP IP PTZ camera.jfif',
    title: '536 2MP IP PTZ camera',
    desc: ``,
    prize: 513.60,
    type: 'kamera',
    sold: 1
  },
  {
    img: './images/Multimetr/JVS-A835-YWC.jfif',
    title: 'JVS-A835-YWC',
    desc: `2MP AHD BULLET CAM, PLASTIC, PAL`,
    prize: 16.59,
    type: 'kamera',
    sold: 4
  },
  {
    img: './images/Multimetr/JVS-A811-BT.jfif',
    title: 'JVS-A811-BT',
    desc: `2MP AHD BULLET CAM, METAL, PAL`,
    prize: 21.75,
    type: 'kamera',
    sold: 4
  },
  
  
  
  //---STABILIZATOR---
  {
    img: './images/Multimetr/stabilizator 500W.jpg',
    title: 'Cтабилизатор напряжения 110V-250V SVC-500W',
    desc: `Автоматический регулятор напряжения (стабилизатор напряжения) 110V-250V SVC-500W`,
    prize: 38.25,
    type: 'stabilizator',
    sold: 32
  },
  {
    img: './images/Multimetr/stabilizator 1000W.jpg',
    title: 'Cтабилизатор напряжения 110V-250V SVC-1000W',
    desc: 'Автоматический регулятор напряжения (стабилизатор напряжения) 110V-250V SVC-1000W',
    prize: 44.28,
    type: 'stabilizator',
    sold: 37
  },
  {
    img: './images/Multimetr/stabilizator 2000W.jpg',
    title: 'Cтабилизатор напряжения 110V-250V SVC-2000W',
    desc: 'Автоматический регулятор напряжения (стабилизатор напряжения) 110V-250V SVC-2000W',
    prize: 69.25,
    type: 'stabilizator',
    sold: 58
  },
  {
    img: './images/Multimetr/stabilizator 2000W.jpg',
    title: 'Cтабилизатор напряжения 110V-250V SVC-3000W',
    desc: 'Автоматический регулятор напряжения (стабилизатор напряжения) 110V-250V SVC-3000W',
    prize: 80.59,
    type: 'stabilizator',
    sold: 1
  },
  {
    img: './images/Multimetr/stabilizator 2000W.jpg',
    title: 'Cтабилизатор напряжения 110V-250V SVC-5000W',
    desc: 'Автоматический регулятор напряжения (стабилизатор напряжения) 110V-250V SVC-5000W',
    prize: 80.59,
    type: 'stabilizator',
    sold: 1
  },
  {
    img: './images/Multimetr/stabilizator 2000W.jpg',
    title: 'Cтабилизатор напряжения 110V-250V SVC-10000W',
    desc: 'Автоматический регулятор напряжения (стабилизатор напряжения) 110V-250V SVC-10000W',
    prize: 80.59,
    type: 'stabilizator',
    sold: 1
  },
  {
    img: './images/Multimetr/stabilizator 2000W.jpg',
    title: 'Cтабилизатор напряжения 110V-250V SVC-20000W',
    desc: 'Автоматический регулятор напряжения (стабилизатор напряжения) 110V-250V SVC-20000W',
    prize: 80.59,
    type: 'stabilizator',
    sold: 1
  },
  {
    img: './images/Multimetr/stabilizator 2000W.jpg',
    title: 'Cтабилизатор напряжения 110V-250V SVC-30000W',
    desc: 'Автоматический регулятор напряжения (стабилизатор напряжения) 110V-250V SVC-30000W',
    prize: 511.76,
    type: 'stabilizator',
    sold: 1
  },
  {
    img: './images/Multimetr/stabilizator SDW-500W.jpg',
    title: 'Cтабилизатор напряжения 110V-250V SDW-500W SDW-0,5KV',
    desc: 'Автоматический регулятор напряжения (стабилизатор напряжения) 110V-250V SDW-500W SDW-0,5KV',
    prize: 52.21,
    type: 'stabilizator',
    sold: 1
  },
  {
    img: './images/Multimetr/stabilizator SDW-500W.jpg',
    title: 'Cтабилизатор напряжения 110V-250V SDW-1000W SDW-1KV',
    desc: 'Автоматический регулятор напряжения (стабилизатор напряжения) 110V-250V SDW-1000W SDW-1KV',
    prize: 54.73,
    type: 'stabilizator',
    sold: 1
  },
  {
    img: './images/Multimetr/stabilizator SDW-500W.jpg',
    title: 'Cтабилизатор напряжения 110V-250V SDW-2000W SDW-2KV',
    desc: 'Автоматический регулятор напряжения (стабилизатор напряжения) 110V-250V SDW-2000W SDW-2KV',
    prize: 69.36,
    type: 'stabilizator',
    sold: 1
  },
  {
    img: './images/Multimetr/stabilizator SDW-500W.jpg',
    title: 'Cтабилизатор напряжения 110V-250V SDW-3000W SDW-3KV',
    desc: 'Автоматический регулятор напряжения (стабилизатор напряжения) 110V-250V SDW-3000W SDW-3KV',
    prize: 103.66,
    type: 'stabilizator',
    sold: 1
  },
  {
    img: './images/Multimetr/stabilizator SDW-500W.jpg',
    title: 'Cтабилизатор напряжения 110V-250V SDW-5000W SDW-5KV',
    desc: 'Автоматический регулятор напряжения (стабилизатор напряжения) 110V-250V SDW-5000W SDW-5KV',
    prize: 126.83,
    type: 'stabilizator',
    sold: 1
  },


  //---MUZIKALNIY USILITEL---
  {
    img: './images/Multimetr/LA012.jfif',
    title: 'LA012',
    desc: '12inch trolley speaker with bluetooth, Recording, FM 6600mAh rechargable battery, USB, TF, Aux, 1 wireless mic, Brown box',
    prize: 87.87,
    type: 'usilitel',
    sold: 1
  },
  {
    img: './images/Multimetr/SL10-01.jfif',
    title: 'SL10-01',
    desc: '10inch trolley speaker with bluetooth, Recording, FM 5.5Ah lead acid rechargable battery, USB, TF, Aux, 2 wireless mic, Brown box',
    prize: 132.24,
    type: 'usilitel',
    sold: 1
  },
  {
    img: './images/Multimetr/QX-1229.jfif',
    title: 'QX-1229',
    desc: '12inch trolley speaker with bluetooth, Recording, FM 4.5Ah rechargable battery, USB, TF, Aux, with two wireless mic, Brown box',
    prize: 145.64,
    type: 'usilitel',
    sold: 1
  },
  {
    img: './images/Multimetr/QX0809.jfif',
    title: 'QX0809',
    desc: '8 inch trolley speaker with bluetooth, Recording, FM 5.5Ah lead acid rechargable battery, USB, TF, Aux, with two wireless Mic, Brown box',
    prize: 118.49,
    type: 'usilitel',
    sold: 1
  },
  {
    img: './images/Multimetr/TRC1400W.jfif',
    title: 'TRC1400W',
    desc: 'Dual 12" Woofer, with bluetooth, Recording, FM 5Ah lead acid rechargable battery, led light, let display, USB, TF, Aux, in&out, with one wireless Mic and one wired Mic Gift box',
    prize: 187.92,
    type: 'usilitel',
    sold: 1
  },
] 

const sliderGoods = [
  {
    img: './images/Multimetr/Multimetr DT-9205A.jfif',
    title: 'Multimetr DT-9205A',
    desc: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi quaerat nobis illo laborum explicabo vero recusandae quos esse sed! `,
    prize: 15.99,
    type: 'multimetr',
    sold: 321
  },
  {
    img: './images/Multimetr/LAN CABLE RG45 CAT6 1M-10M.jfif',
    title: 'LAN CABLE RG45 CAT6 2M',
    desc: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi quaerat nobis illo laborum explicabo vero recusandae quos esse sed! `,
    prize: 0.79,
    type: 'kabel',
    sold: 5
  },
  {
    img: './images/Multimetr/JVS-A815-C1.jfif',
    title: 'JVS-A815-C1 2MP AHD DOME CAM, PLASTIC, PAL',
    desc: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi quaerat nobis illo laborum explicabo vero recusandae quos esse sed! `,
    prize: 16.59,
    type: 'kamera',
    sold: 4
  },
  {
    img: './images/Multimetr/stabilizator 500W.jpg',
    title: 'Автоматический регулятор напряжения (стабилизатор напряжения) 110V-250V SVC-500W',
    desc: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi quaerat nobis illo laborum explicabo vero recusandae quos esse sed! `,
    prize: 38.25,
    type: 'stabilizator',
    sold: 32
  },
  {
    img: './images/Multimetr/LA012.jfif',
    title: 'LA012',
    desc: '12inch trolley speaker with bluetooth, Recording, FM 6600mAh rechargable battery, USB, TF, Aux, 1 wireless mic, Brown box',
    prize: 87.87,
    type: 'usilitel',
    sold: 1
  }
]
const sectionCenter = document.querySelector('.section-center');

const filterBtns = document.querySelectorAll('.filter-btn');


// -----------OPEN MODAL ----
//------------DISPLAY ITEMS----------------------------
window.addEventListener('DOMContentLoaded', function() {
  displayGoodsItems(goods);
  // displaySliderItems(goods.slice(0,6))
  displaySliderItems(sliderGoods)
})

function displayGoodsItems(goodsItems){
  let displayGoods = goodsItems.map(function(item){
    return `
    <div class="card">
      <img src='${item.img}' alt="${item.title}">
      <div class="card-body">
        <h2 class="cart-title">${item.title}</h2>
        <p class="cart-text">${item.desc}</p>
        <div>
          <h5 class="cart-text">$${item.prize}</h5>
          <h5 class="cart-num">Продано: ${item.sold}</h5>
        </div>      
      </div>
    </div>
    `
  })
  sectionCenter.innerHTML = displayGoods.join('')
}

//------------FILTER ITEMS--------------------------
filterBtns.forEach(function(btn){
  btn.addEventListener('click', function(e){
    const type = e.currentTarget.dataset.id;
    // console.log(type)
    const goodsType = goods.filter(function(goodsItem){
      // console.log(goodsItem.type)
      if(goodsItem.type === type){
        return goodsItem;
      }
    });
    // console.log(type === "all");
    if(type === "all"){
      displayGoodsItems(goods);
    } else {
      displayGoodsItems(goodsType);
    }
  })
})
//-------------SLIDER------------------

        let slider = document.querySelector(".slider-center");
        const nextBtn = document.querySelector(".fa-chevron-right");
        const prevBtn = document.querySelector(".fa-chevron-left");

        //slide
        let boxWidth = 500;
        nextBtn.addEventListener("click", () => {
          slider.scrollBy({ left: boxWidth, behavior: "smooth" });
        });
        prevBtn.addEventListener("click", () => {
          slider.scrollBy({ left: -boxWidth, behavior: "smooth" });
        });

        function displaySliderItems(sliderItem){
          let sliderGoods = sliderItem.map(function(item){
            return `
            <div class="slide-box">
              <div class="infor">
                <img src='${item.img}' alt="${item.title}"> 
                <div class="make-center">
                  <h7>${item.title}</h7>
                  <h8>$${item.prize}</h8>
                  <p>Продано: ${item.sold}</p>
                </div>
              </div>
              <div class="p">
                <p>${item.desc}</p>
              </div>
            </div>
            `
          })
          slider.innerHTML = sliderGoods.join('')
        }

        // ---KOLLEKSIYA TOVARI---
        const tovari = document.querySelector('.tovari');
        const kameri = document.querySelector('.kameri');
        const kabel = document.querySelector('.kabel');
        const usilitel = document.querySelector('.usilitel');

        window.addEventListener('DOMContentLoaded', function(){
          displayTovari(goods)
          displovari(goods)
          displayT(goods)
          displayUsilit(goods)
        })
       
        function displayTovari(collection){
          let displayTov = collection.map(function(tovar){
            if(tovar.type === 'multimetr') 
            return `
            <div class="collection">
            <img src="${tovar.img}" alt="${tovar.title}">
            <h2>${tovar.title}</h2>
            <div>
            <h4>$${tovar.prize}</h4>
            <h4>Продано: ${tovar.sold}</h4>
            </div>
            </div>
            `
          })
          displayTov = displayTov.join('')
          tovari.innerHTML = displayTov
        }
        
        function displovari(collection){
          let displayKameri = collection.map(function(tovar){
            if(tovar.type === 'kamera') 
            return `
            <div class="collection">
            <img src="${tovar.img}" alt="${tovar.title}">
            <h2>${tovar.title}</h2>
            <div>
            <h4>$${tovar.prize}</h4>
            <h4>Продано: ${tovar.sold}</h4>
            </div>
            </div>
            `
          })
          kameri.innerHTML = displayKameri.join('')
        }
      
        function displayT(collection){
          let displayKabel = collection.map(function(tovar){
            if(tovar.type === 'kabel') 
            return `
            <div class="collection">
              <img src="${tovar.img}" alt="${tovar.title}">
              <h2>${tovar.title}</h2>
              <div>
                <h4>$${tovar.prize}</h4>
                <h4>Продано: ${tovar.sold}</h4>
              </div>
            </div>
            `
          })
          kabel.innerHTML = displayKabel.join('')
        }
        
        function displayUsilit(collection){
          let displayUsilitel = collection.map(function(tovar){
              if(tovar.type === 'usilitel')
                return `
                <div class="collection">
                    <img src="${tovar.img}" alt="${tovar.title}">
                    <h2>${tovar.title}</h2>
                  <div>
                    <h4>$${tovar.prize}</h4>
                    <h4>Продано: ${tovar.sold}</h4>
                  </div>
                </div>
                ` 
          })
          usilitel.innerHTML = displayUsilitel.join('')
        }